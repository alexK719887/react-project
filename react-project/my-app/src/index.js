import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Counter} from './Components/Counter/Counter.js';


ReactDOM.render(
	<Counter/>,
	 document.getElementById('root'));
registerServiceWorker();
