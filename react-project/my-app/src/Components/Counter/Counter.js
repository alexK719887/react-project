import React from 'react';
import './Counter.css';
import {MainButton} from '../Button/MainButton.js';


export class Counter extends React.Component {

  constructor (props) {
    super (props);
    this.state = {count:0};
    };



    _plusValue = () => {
    this.setState({count: this.state.count + 1});
};
  _minusValue = () => {
    this.setState({count: this.state.count - 1});
    };

    _changeValue = (event) => {
      console.log('event', event);
      console.log('event.currentTarget', event.currentTarget);
        if (event.currentTarget.type === "плюс") {
        this.setState({count: this.state.count + 1})
      } else {
      this.setState({count: this.state.count - 1})
      }
    }



    render() {

      console.log(MainButton);
      return(
        <div className='block'>
        <div className="app-header">
        <div className="app-count">{this.state.count}</div>
        </div>
        <div>

        <MainButton color='red' name="плюс" onClick = {this._changeValue}> нажми + </MainButton>
      <MainButton color='blue' name="минус" onClick = {this._changeValue}> нажми - </MainButton>
        </div>
        </div>
        )
      }
    }
