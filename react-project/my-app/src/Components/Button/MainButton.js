import React, { Component } from 'react';
import './Button.css';

export class MainButton extends React.Component {


	render(){
		console.log(this.props)
		return (
			<div>
				<button onClick={this.props.onClick} style={{background: this.props.color}} type={this.props.name}>
					{this.props.children}
				</button>
			</div>
				)
	}
}
